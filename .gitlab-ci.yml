image: debian:buster

stages:
 - deploy
 - production

variables:
  LC_ALL: C.UTF-8
  DEBIAN_FRONTEND: noninteractive
  OUT_DIR: build

# This template needs to be in text block format since gitlab-runner
# exec cannot handling templates in list format.
.apt-template: &apt-template |
  set -x
  set -e
  echo Etc/UTC > /etc/timezone
  echo 'quiet "1";' \
       'APT::Install-Recommends "0";' \
       'APT::Install-Suggests "0";' \
       'APT::Acquire::Retries "20";' \
       'APT::Get::Assume-Yes "true";' \
       'Dpkg::Use-Pty "0";' \
      > /etc/apt/apt.conf.d/99gitlab
  echo "deb http://deb.debian.org/debian/ buster-backports main" >> /etc/apt/sources.list
  printf "Package\x3a ruby-git ruby-jekyll-include-cache ruby-jekyll-last-modified-at ruby-jekyll-paginate-v2 ruby-jekyll-redirect-from ruby-jekyll-sitemap ruby-loofah ruby-nokogiri ruby-rchardet ruby-rouge ruby-zip\nPin\x3a release a=buster-backports\nPin-Priority\x3a 500\n" > /etc/apt/preferences.d/debian-buster-backports.pref
  apt-get update
  apt-get dist-upgrade


# Common steps required for each type of "Build" (f-droid.org, GitLab Pages, feature branches)
#
# This template needs to be in text block format since gitlab-runner
# exec cannot handling templates in list format.
.setup_for_jekyll: &setup_for_jekyll |
  set -x
  set -e
  apt-get install --install-recommends git libunicode-linebreak-perl po4a
  apt-get purge po4a
  git -C $HOME clone https://github.com/mquinson/po4a.git
  export PERLLIB="$HOME/po4a/lib"
  export PATH="$HOME/po4a:$PATH"
  apt-get install \
    gettext \
    linkchecker \
    python3-babel \
    rsync \
    rubocop \
    ruby-bundler \
    ruby-git \
    ruby-jekyll-include-cache \
    ruby-jekyll-paginate-v2 \
    ruby-jekyll-redirect-from \
    ruby-json \
    ruby-loofah \
    ruby-nokogiri \
    ruby-rchardet \
    ruby-rouge \
    ruby-rspec \
    ruby-zip \
    unzip
  bundle install --local --verbose
  ./tools/i18n.sh po2md


# This is a manual task for building in preparation to deploy to
# https://f-droid.org. The intention is for it to be run locally using
# `gitlab-runner` each time a tag is found that is signed by a key in
# the whitelist keyring.  Invoke like so:
#
#  gitlab-runner exec docker f-droid.org --pre-build-script ./prepare-for-deploy.py \
#    --docker-volumes "/root/deploy-whitelist-keyring.gpg:/root/.gnupg/pubring.gpg:ro" \
#    --docker-volumes `pwd`/_site:/builds/output
#
# And when it is finished, you should have a directory in _site/build/
# which includes the entire static site ready to be deployed to
# https://f-droid.org.
f-droid.org:
  stage: production
  only:
    - tags@fdroid/fdroidserver
    - master@fdroid/fdroidserver
  when: manual
  script:
   - '[ ! -d /builds/output ] && echo "ERROR: /builds/output is not mounted inside docker!" && exit 1'
   - *apt-template
   - *setup_for_jekyll
   - 'echo "url: https://f-droid.org" > _userconfig.yml'
   - 'echo "baseurl: \"\"" >> _userconfig.yml'
   - echo "Additional Jekyll config used for CI:" && cat _userconfig.yml
   - jekyll build -d $OUT_DIR --config _config.yml,_userconfig.yml --trace
   - ./tools/prepare-multi-lang.sh $OUT_DIR
   - ./tools/deploy-external-assets.sh $OUT_DIR
   - rsync -ax --delete $OUT_DIR /builds/output/


pages:
  stage: deploy
  except:
    - triggers
  artifacts:
    paths:
      - public
    expire_in: 1w
    when: always
  script:
   - *apt-template
   - apt-get install curl python3-yaml
   - ./tools/trigger-spellcheckbot
   # use the 'gitlab ci' subset of languages
   - sed -i 's,^languages:,ignored_languages:,' _config.yml
   - sed -i 's,^gitlab_ci_languages:,languages:,' _config.yml
   - *setup_for_jekyll
   - ./tools/check-format-strings.py
   - ./tools/check-page-links.py
   - ./tools/check-yaml-front-matter.py
   - ./tools/check-do-not-translate
   - ./tools/check-markdown-headers-are-localizable.py
   - ./tools/i18n.sh md2po
   - git checkout po/*.pot  # ignore the newly generated timestamp
   - git --no-pager diff --ignore-all-space --name-only po/
   # This is where GitLab pages will deploy to by default (e.g. "https://fdroid.gitlab.io/fdroid-website")
   # so we need to make sure that the Jekyll configuration understands this.
   - 'echo url: https://$CI_PROJECT_NAMESPACE.gitlab.io > _userconfig.yml'
   - 'echo baseurl: /$CI_PROJECT_NAME >> _userconfig.yml'
   - echo "Additional Jekyll config used for CI:" && cat _userconfig.yml
   - jekyll build -d public --config _config.yml,_userconfig.yml --trace
   - ./tools/prepare-multi-lang.sh public --no-type-maps
   - mkdir linkchecker/
   - ln -s ../public linkchecker/$CI_PROJECT_NAME
   - ruby -run -e httpd linkchecker/ -p 4000 > /dev/null 2>&1 &
   - linkchecker http://localhost:4000/$CI_PROJECT_NAME --config=.linkcheckerrc


spellcheckbot:
  image: node:buster
  stage: deploy
  allow_failure: true
  only:
    - triggers
  script:
    - test -n ${FROM_CI_PROJECT_URL}
    - test -n ${FROM_CI_COMMIT_SHA}
    - git fetch ${FROM_CI_PROJECT_URL} ${FROM_CI_COMMIT_SHA}
    - for f in `git diff --name-only --diff-filter=d HEAD...${FROM_CI_COMMIT_SHA}`; do
          export CHANGED="$CHANGED `echo $f | grep '\.md$' || true`";
      done
    - if [ -z "`echo $CHANGED | sed 's,\s*,,g'`" ]; then
          echo "No markdown files changed";
          exit 0;
      else
          echo "Spellchecking $CHANGED";
          git checkout --force ${FROM_CI_COMMIT_SHA};
      fi
    - apt-get -qy update
    - apt-get -qy install --no-install-recommends --allow-unauthenticated python3-gitlab python3-requests
    - ./tools/fetch-spelling-words.py >> .spelling
    - npm i markdown-spellcheck -g
    - (mdspell --report --en-gb --ignore-numbers --ignore-acronyms $CHANGED > output.txt 2>&1)
        || ./tools/spellcheckbot.py
    - cat output.txt


# Download and verify that the FDroid.apk is signed by the right PGP
# key.  The only time that F-Droid's signed metadata does not verify
# the APK is the initial download and install of F-Droid itself.  An
# attacker could replace the FDroid.apk and PGP signature on the
# website. The gpg key model is to trust only the key that is included
# in this script, so there is a test to check that it is starting with
# an empty keyring.

check_fdroid_apk_bot:
  stage: deploy
  only:
    - schedules
    - master@fdroid/fdroidserver
  image: alpine:3.5
  variables:
    apk: F-Droid.apk
    asc: F-Droid.apk.asc
    curl: "curl --silent --user-agent F-Droid --retry 99"
    fingerprint: 37D2C98789D8311948394E3E41E7044E1DBA2E89
    pip: pip3 --timeout 100 --retries 10
  artifacts:
    name: "$apk-failed-${CI_JOB_ID}"
    paths:
      - $apk
      - $asc
    expire_in: 180 days
    when: on_failure
  script:
    - apk add --no-cache gnupg curl
    - "! (gpg --list-keys | grep pub)"
    - while ! gpg --keyserver pgp.mit.edu --recv-key $fingerprint; do sleep 10; done
    - gpg --list-key --fingerprint | tr -d '[:space:]' | grep $fingerprint
    - echo "${fingerprint}:6:" | gpg --import-ownertrust
    - $curl https://f-droid.org/$apk > $apk
    - $curl https://f-droid.org/$asc > $asc
    - sha256sum $apk
    - gpg --batch --trust-model always --verify $asc $apk || (
          apk add --no-cache python3;
          python3 -m ensurepip;
          $pip install python-gitlab;
          ./tools/check-fdroid-apk-bot.py;
      )


i18n_sync:
  stage: deploy
  except:
    - triggers
  only:
    - tags
    - weblate
  script:
    - *apt-template
    - apt-get install bash gettext git grep po4a sed
    - ./tools/i18n.sh updatepo
    - git checkout po/*.pot  # ignore the newly generated timestamp
    - git add po/*.po
    - git config user.email "you@example.com"
    - git config user.name "DO NOT MERGE"
    - git --no-pager diff --exit-code || git commit po/*.po -m "DO NOT MERGE sort before test"
    - ./tools/i18n.sh md2po
    - ./tools/i18n.sh updatepo
    - git checkout po/*.pot  # ignore the newly generated timestamp
    - git --no-pager diff --exit-code --ignore-all-space -G'^msg' po/
        || (echo 'This test failed because the localization files were not synced.  To do that, run `./tools/i18n.sh md2po` then commit the changes in po/ and include them in this merge request.'; exit 1)
  allow_failure: true
